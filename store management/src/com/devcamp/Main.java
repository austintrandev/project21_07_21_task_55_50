package com.devcamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //TODO 1: 3 dong duoi day lam gi 
	    Staff p_huy = new Staff("Huy", "Pham", 20, "Ha Noi", "Boi ban", 80000);
        Staff p_tu = new Staff("Tu", "Nguyen", 21, "Ha Noi", "Le Tan", 100000);
        Staff p_giang = new Staff("Giang", "Tran", 40, "Ha Noi", "Bao ve", 100000);

        //TODO 2: 4 dong duoi day lam gi
        List<Staff> p_staffList = new ArrayList<Staff>();
        p_staffList.add(p_huy);
        p_staffList.add(p_tu);
        p_staffList.add(p_giang);

        //TODO 3: Dong duoi day lam gi
        List<Customer> p_customerList = new ArrayList<Customer>();

        //TODO 4: Dong duoi day lam gi
        Date p_date = new Date();

        //TODO 5: Dong duoi day lam gi
        Store p_store = new Store(p_staffList, p_customerList, p_date, 500000);

        //TODO 6: Dong duoi day lam gi
        Staff p_tien = new Staff("Tien", "Tran", 18, "Ha Noi", "Phuc vu parttime", 40000);

        //TODO 7: Dong duoi day lam gi
        p_store.addStaff(p_tien);

        //TODO 8: Dong duoi day lam gi
        p_store.updateTotalMoneySpent();

        //TODO 9: 2 dong duoi day lam gi
        int p_salarySpent = p_store.getTotalMoneySpent();
        System.out.println("Total slary spent: " + p_salarySpent);

        //TODO 10: 4 dong duoi day lam gi
        Customer p_thanh = new Customer("Thanh", "Pham", 18, "Ha Noi", 200000);
        Customer p_dung = new Customer("Dung", "Vu", 17, "Bac Ninh", 250000);
        Customer p_van = new Customer("Van", "Pham", 20, "Ha Nam", 250000);
        Customer p_nam = new Customer("Nam", "Dinh", 20, "Thai Binh", 500000);

        //TODO 11: 4 dong duoi day lam gi
        p_store.addCustomer(p_thanh);
        p_store.addCustomer(p_dung);
        p_store.addCustomer(p_van);
        p_store.addCustomer(p_nam);

        //TODO 12: Dong duoi day lam gi 
        p_store.updateTotalMoneyEarned();

        //TODO 13: 2 dong duoi day lam gi
        int p_moneyEarned = p_store.getTotalMoneyEarned();
        System.out.println("Total money earned: " + p_moneyEarned);

        //TODO 14: Dong duoi day lam gi
        System.out.println(p_store.toString());
    }
}
